#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <libgen.h>
#include <string.h>

void write_json_info(const char *nume_director_fisier, struct stat st, FILE *file, int level)
{
    char formattedLastAccess[26];
    char formattedLastModification[26];
    char indent[256] = {0};
    for (int i = 0; i <= level * 4; i++)
    {
        indent[i] = ' ';
    }

    // Formatăm datele pentru a elimina newline-ul generat de ctime
    strncpy(formattedLastAccess, ctime(&st.st_atime), 24);
    formattedLastAccess[24] = '\0';
    strncpy(formattedLastModification, ctime(&st.st_mtime), 24);
    formattedLastModification[24] = '\0';

    // Scriem informațiile directorului în fișier
    fprintf(file, "%s\"Dimensiune\": %ld,\n", indent, st.st_size);
    fprintf(file, "%s\"Data ultimei accesări\": \"%s\",\n", indent, formattedLastAccess);
    fprintf(file, "%s\"Data ultimei modificări\": \"%s\"\n", indent, formattedLastModification); // Acum corect terminat
}

void functie_director(char *nume_director_fisier, DIR *dir, FILE *file, int level)
{
    struct dirent **namelist;
    int n = scandir(nume_director_fisier, &namelist, NULL, alphasort);
    if (n < 0)
    {
        perror("scandir");
        exit(EXIT_FAILURE);
    }

    char indent[256] = {0};
    memset(indent, ' ', level * 4);

    struct stat st;
    if (lstat(nume_director_fisier, &st) == -1)
    {
        perror("Eroare la obținerea informațiilor despre director");
        exit(1);
    }

    char *dir_name = basename(nume_director_fisier);
    fprintf(file, "%s\"%s\": {\n", indent, dir_name);
    write_json_info(nume_director_fisier, st, file, level);

    int i;
    for (i = 0; i < n; i++)
    {
        struct dirent *entry = namelist[i];
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
            continue;

        char path[1024];
        snprintf(path, sizeof(path), "%s/%s", nume_director_fisier, entry->d_name);
        if (lstat(path, &st) == -1)
        {
            perror("lstat");
            exit(EXIT_FAILURE);
        }

        if (i > 0)
            fprintf(file, ",\n");

        if (S_ISDIR(st.st_mode))
        {
            DIR *subdir = opendir(path);
            if (subdir)
            {
                functie_director(path, subdir, file, level + 1);
                closedir(subdir);
            }
        }
        else if (S_ISREG(st.st_mode))
        {
            fprintf(file, "%s\"%s\": {\n", indent, entry->d_name);
            write_json_info(path, st, file, level + 1);
            fprintf(file, "\n%s}", indent);
        }
        free(namelist[i]);
    }
    free(namelist);

    fprintf(file, "\n%s}", indent);
}

char *cauta_dir_iesire(int argc, char *argv[])
{
    for (int i = 1; i < argc; i++)
    {
        if (strcmp(argv[i], "-o") == 0)
            return argv[i + 1];
    }
    return NULL;
}

int main(int argc, char *argv[])
{

    char *dir_iesire = cauta_dir_iesire(argc, argv);

    FILE *file = fopen(strcat(dir_iesire, "/date.json"), "w");
    if (file == NULL)
    {
        perror("Eroare la deschiderea fișierului");
        return 1;
    }

    fprintf(file, "{\n");

    for (int i = 1; i < argc; i++)
    {

        if (strcmp(argv[i], "-o") == 0)
        {
            i = i + 2;
        }
        if (i < argc)
        {
            DIR *dir = opendir(argv[i]);
            if (dir == NULL)
            {
                fprintf(stderr, "Eroare la deschiderea directoruluui %s\n", argv[i]);
                fclose(file);
                return 1;
            }

            functie_director(argv[i], dir, file, 0);
            if (i < argc - 3)
                fprintf(file, "\n,");
            closedir(dir);
        }
    }
    fprintf(file, "\n}");

    fclose(file);

    return 0;
}
